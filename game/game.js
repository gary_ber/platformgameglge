var canvas = document.getElementById( 'canvas' );
//resize canvas to fit screen
canvas.width=innerWidth; 
canvas.height=innerHeight; 
window.onresize = function(){
    canvas.width=innerWidth; 
    canvas.height=innerHeight; 
}
//disable right click on canvas
canvas.oncontextmenu=function(e){
	return false;
}

var renderer = new GLGE.Renderer( canvas );
var XMLdoc = new GLGE.Document();
var keys = new GLGE.KeyInput(); // monitor key presses
var mouse = new GLGE.MouseInput(canvas); //monitor mouse on canvas

var scene;
var camera;
var level;

var playerBasket = new Array();
var evilBasket = new Array();

XMLdoc.onLoad = function(){	
    scene = XMLdoc.getElement("mainscene");
    camera = XMLdoc.getElement("mainCamera");
    level = XMLdoc.getElement("level");
    
    //player basket
    for(i=0; i<2; i++)
    {
        playerBasket[i] = new player();
    };
    var you = playerBasket[0];
    //evil basket
    for(i=0; i<5; i++)
    {
        evilBasket[i] = new evilcube(5*i);
    };
    
    //sets scene to use
	renderer.setScene( scene ); 
    //this is the main game loop
    renderer.render();
	setInterval(function(){
	    you.checkKeys();
        you.checkMouse();
        you.fallow();
        scene.physicsTick(0.035); // integrate physics sim
        renderer.render(); //render the scene
	},15);
};
XMLdoc.load("game/scene/game.xml");