player=function()
{
    //model
    var ogroColor=new GLGE.Texture({Src: "game/assets/ogro/skin.jpg"});
    var ogroMaterial=new GLGE.Material({Specular: 0})
	.addMaterialLayer(new GLGE.MaterialLayer({Texture: ogroColor, Mapinput: GLGE.UV1, Mapto: GLGE.M_COLOR}))
	.addTexture(ogroColor);
    var player_model=new GLGE.MD2({
    		Src:"game/assets/ogro/ogro.md2",
    		Material: ogroMaterial,
    		MD2Animation: "stand",
    		Scale: 0.15,
    		LocZ: 3,
    		RotZ: 1.57
    });
    this.model = player_model;
    //physics feet
    var player_physics_feet = new GLGE.PhysicsSphere;
    player_physics_feet.setLocX(0);
    player_physics_feet.setLocY(0);
    player_physics_feet.setLocZ(10);
    player_physics_feet.addObject(player_model);
    player_physics_feet.setFriction(0);
    player_physics_feet.setRotationalVelocityDamping([0,0,0]); 
	player_physics_feet.setLinearVelocityDamping([0,0.9,0.995]); 
    scene.addChild(player_physics_feet);
    this.physics_feet = player_physics_feet;
    //physics body
    var player_physics_body = new GLGE.PhysicsSphere;
    player_physics_body.setLocX(0);
    player_physics_body.setLocY(0);
    player_physics_body.setLocZ(15);
    player_physics_body.setRadius(1);
    //player_physics_body.addObject(player_model);
    player_physics_body.setFriction(0);
    player_physics_body.setRotationalVelocityDamping([0,0,0]); 
	player_physics_body.setLinearVelocityDamping([0,0.9,0.995]); 
    scene.addChild(player_physics_body);
    this.physics_body = player_physics_body;
    //make player parts non collidable
    player_physics_feet.jigLibObj._nonCollidables=[player_physics_feet.jigLibObj, player_physics_body.jigLibObj];
    player_physics_body.jigLibObj._nonCollidables=[player_physics_feet.jigLibObj, player_physics_body.jigLibObj];
    //name text
    var player_name = new GLGE.Text({
        Text: "Player",
        Font: "arial",
        Color: "red",
        Size: 100,
        LocX: 10,
        RotX: 3,
        RotY: -.5,
        RotZ: 1.4
    });
    scene.addChild(player_name);
    this.name_text = player_name;
    //health text
    var player_health = new GLGE.Text({
        Text: this.health,
        Font: "arial",
        Color: "blue",
        Size: 100,
        LocX: 10,
        RotX: 3,
        RotY: -.5,
        RotZ: 1.4
    });
    scene.addChild(player_health);
    this.player_health = player_health;
    this.health = 100;
    this.onground = false;
    this.jumped = false;
    this.currentAnimation;
    this.direction;
    this.collision(this);
    this.stick(this);
};
player.prototype.stick = function(instance)
{
    var sticky = setInterval(function(e){
        instance.name_text.setLocY(instance.physics_feet.getLocY());
        instance.name_text.setLocZ(11+instance.physics_feet.getLocZ());
        var h;
        if(instance.health<0)
        {
            h=0;
        }
        else
        {
            h=instance.health;
        };
        instance.player_health.setText(h);
        instance.player_health.setLocY(instance.physics_feet.getLocY());
        instance.player_health.setLocZ(10+instance.physics_feet.getLocZ());
    }, 15);
}
player.prototype.collision = function(instance)
{
    instance.physics_feet.addEventListener("collision", function(e){
        switch(e.obj)
        {
            case level:
                instance.onground = true;
            break;
        };
    });
    instance.physics_body.addEventListener("collision",function(e){
        instance.physics_body.jigLibObj.applyBodyWorldImpulse([e.impulse[0]*3,e.impulse[1]*3,e.impulse[2]]*3,[0,0,3]); 
        console.log("body collided");
    });
}
player.prototype.checkKeys = function()
{
    var anim;
    if(keys.isKeyPressed(GLGE.KI_W))
    {
        if(!this.jumped && this.onground)
        {
            this.jumped=true;
            this.onground=false;
            this.physics_feet.setVelocityZ(15);
            anim = "jump";
            if(this.currentAnimation != anim)
            {
                this.model.setMD2Animation(anim, false);   
                this.currentAnimation = anim; 
            };
        };
    }
    else
    {
        if(this.onground)
        {
            this.jumped = false;    
        };
    };
    if(keys.isKeyPressed(GLGE.KI_S))
    {
        if(keys.isKeyPressed(GLGE.KI_A)) //move left
        {
            anim = 'cwalk'; 
            this.physics_feet.setVelocityY(-5);
        }
        else if(keys.isKeyPressed(GLGE.KI_D)) //move right
        {
            anim = 'cwalk';  
            this.physics_feet.setVelocityY(5);
        }
        else
        {
            anim = 'cstand';
        };
        if(this.onground)
        {
            if(this.currentAnimation != anim)
            {
                this.model.setMD2Animation(anim, true);    
                this.currentAnimation = anim;
            };
        };
    }
    else //standing
    {
        if(keys.isKeyPressed(GLGE.KI_A)) //move left
        {
            anim = 'run';
            if(this.direction=="left")
            {
                this.physics_feet.setVelocityY(-10);
            }
            else if(this.direction=="right")
            {
                this.physics_feet.setVelocityY(-5);
            };
        }
        else if(keys.isKeyPressed(GLGE.KI_D)) //move right
        {
            anim = 'run';
            if(this.direction=="right")
            {
                this.physics_feet.setVelocityY(+10);
            }
            else if(this.direction=="left")
            {
                this.physics_feet.setVelocityY(+5);
            };
        }
        else
        {
            anim = 'stand';
        };
        if(this.onground)
        {
            if(this.currentAnimation != anim)
            {
                this.model.setMD2Animation(anim, true);    
                this.currentAnimation = anim;
            };
        };
    };
};
var thrown = false;
player.prototype.checkMouse = function()
{
    if(mouse.isButtonDown(0))
    {
        if(thrown == false)
        {
            this.throwBall();
            thrown = true;
        };
    }
    else
    {
        thrown = false;
    };
    //rotate player to direction of mouse
    var mousepos = mouse.getMousePosition();
    var x = mousepos.x-(canvas.width/2);
    if(x<0)
    {
        //this.model.setRotZ(-1.6);
        this.model.blendTo({RotZ:-1.6}, 100);
        this.direction = "left";
    }
    else if(x>0)
    {
        //this.model.setRotZ(1.6);
        this.model.blendTo({RotZ:1.6}, 100);
        this.direction = "right";
    };
}
player.prototype.throwBall = function()
{
    var ball = new GLGE.Collada;
    ball.setDocument("game/assets/ball/ball.dae");
    var ballPhysics = new GLGE.PhysicsSphere;
    ballPhysics.setLocX(0);
    ballPhysics.setLocY(this.physics_feet.getLocY());
    ballPhysics.setLocZ(6+this.physics_feet.getLocZ());
    ballPhysics.setRotZ(1.57);
    ballPhysics.addObject(ball);
    ballPhysics.setScale(0.5);
    ballPhysics.setRadius(0.3);
	ballPhysics.setFriction(0);
    ballPhysics.addEventListener("collision",function(e){
        if(e.obj==this.physics_feet)
        {
            scene.removeChild(ballPhysics);
        }
        else if(e.obj==level)
        {
            clearInterval(throwTimer);
        };
        for(i=0;i<evilBasket.length;i++)
        {
            if(e.obj==evilBasket[i].physics_feet)
            {
                evilBasket[i].health-=10;
                console.log("evilcube"+i+" hit. "+ evilBasket[i].health);
            };
        };
	});
    scene.addChild(ballPhysics);
    var ballDirection = this.direction;
    var vZ = 0;
    if(ballDirection == "right")
    {
        vZ = ((mouse.getMousePosition().y-(canvas.height/2)+75)/10);
    }
    else if(ballDirection == "left")
    {
        vZ = ((mouse.getMousePosition().y-(canvas.height/2)+75)/10);
    };
    var i = 0;
    var throwTimer = window.setInterval(function(){
        i++;
        ballPhysics.setVelocityX(0);
        ballPhysics.setVelocityZ(-vZ-i);
        if(ballDirection=="right")
        {
            ballPhysics.setVelocityY(20);     
        }
        else if(ballDirection=="left")
        {
            ballPhysics.setVelocityY(-20);     
        };
    }, 100);
    //delete the ball after a set time
    var ballTimeout = window.setTimeout(function(e){
        scene.removeChild(ballPhysics);
        window.clearTimeout(ballTimeout);
    }, 5000);
};
player.prototype.fallow = function()
{
    camera.setLocY(this.physics_feet.getLocY());
    camera.setLocZ(20+this.physics_feet.getLocZ());
    this.physics_body.setLocX(this.physics_feet.getLocX());
    this.physics_body.setLocY(this.physics_feet.getLocY());
    this.physics_body.setLocZ(this.physics_feet.getLocZ()+3);
};