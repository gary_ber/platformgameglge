function evilcube(z)
{
    var evilcube_model = new GLGE.Collada;
    evilcube_model.setDocument("game/assets/evilcube/evilcube.dae");
    evilcube_model.setScale(3);
    evilcube_model.setLocY(-1);
    evilcube_model.setLocZ(1);
    var evilcube_physics = new GLGE.PhysicsSphere;
    evilcube_physics.setLocX(0);
    evilcube_physics.setLocY(20);
    evilcube_physics.setLocZ(15+z);
    evilcube_physics.setRotZ(1.57);
    evilcube_physics.addObject(evilcube_model);
    evilcube_physics.setRadius(0.5);
    evilcube_physics.setRotationalVelocityDamping([0,0,0]); 
	evilcube_physics.setLinearVelocityDamping([0,0.9,0.995]); 
    //evilcube_physics.setRotationalVelocityDamping([0,0,0.9]); 
    //evilcube_physics.setLinearVelocityDamping([0.9,0.9,0.995]); 
    evilcube_physics.setFriction(0);
    scene.addChild(evilcube_physics);
    
    var evilcube_name = new GLGE.Text();
    evilcube_name.setText("EvilCube");
    evilcube_name.setFont("arial");
    evilcube_name.setColor("red");
    evilcube_name.setSize(100);
    evilcube_name.setLocX(0);
    evilcube_name.setRotX(3);
    evilcube_name.setRotY(-.5);
    evilcube_name.setRotZ(1.4);
    scene.addChild(evilcube_name);
    
    var evilcube_health = new GLGE.Text();
    evilcube_health.setText("100");
    evilcube_health.setFont("arial");
    evilcube_health.setColor("blue");
    evilcube_health.setSize(100);
    evilcube_health.setLocX(0);
    evilcube_health.setRotX(3);
    evilcube_health.setRotY(-.5);
    evilcube_health.setRotZ(1.4);
    scene.addChild(evilcube_health);
    
    
    
    this.physics_feet = evilcube_physics;
    this.health = 100;
    
    //--------------evilcube logic--------------
    
    var playerBasketLen = playerBasket.length;
    
    var evilcubeLife = true;
    var evilcubeOnground = false;
    evilcube_physics.addEventListener("collision",function(e){
        if(e.obj==level)
        {
            evilcubeOnground = true;
        };
        for(i=0;i<playerBasketLen; i++)
        {
            if(e.obj == playerBasket[i].physics_feet)
            {
                playerBasket[i].health-=10;
            };
        };
    });
    
    var evilLoop = setInterval(function(e){
        evilcube_name.setLocY(evilcube_physics.getLocY());
        evilcube_name.setLocZ(5+evilcube_physics.getLocZ());
        if(this.health<=0)
        {
            clearInterval(evilLoop);
        };
        //get the closest player
        var goForPlayerId=0;
        var lastDistanceY;
        for(i=0;i<playerBasketLen; i++)
        {
            var distanceY = Math.abs(playerBasket[i].physics_feet.getLocY() - evilcube_physics.getLocY());
            if(distanceY<lastDistanceY)
            {
                goForPlayerId = i;
                console.log(i);
            };
            lastDistanceY=distanceY;
        };
        //negative y = player is on left of cube
        var y = playerBasket[goForPlayerId].physics_feet.getLocY() - evilcube_physics.getLocY();
        if(Math.floor(y))
        {
            if(y<0)
            {
                evilcube_physics.setVelocityY(-5); //left
            }
            else
            {
                evilcube_physics.setVelocityY(5); //right
            };    
        };
        var z = playerBasket[goForPlayerId].physics_feet.getLocZ() - evilcube_physics.getLocZ();
        if(Math.floor(z))
        {
            if(z>0)
            {
                if(evilcubeOnground)
                {
                    evilcube_physics.setVelocityZ(10); //jump
                    evilcubeOnground = false;
                };
            };
        };
    }, 15);
};